PGDMP     *    1                z         
   db_sidilan    15.1    15.1                0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false                       1262    24718 
   db_sidilan    DATABASE     �   CREATE DATABASE db_sidilan WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'English_Indonesia.1252';
    DROP DATABASE db_sidilan;
                postgres    false            �            1259    24965 
   tbl_aturan    TABLE     �   CREATE TABLE public.tbl_aturan (
    id_aturan character varying(4) NOT NULL,
    id_kerusakan character varying(3),
    id_gejala character varying(4),
    id_bobot character varying(3)
);
    DROP TABLE public.tbl_aturan;
       public         heap    postgres    false            �            1259    24968 	   tbl_bobot    TABLE     �   CREATE TABLE public.tbl_bobot (
    id_bobot character varying(3) NOT NULL,
    nilai_bobot real,
    keterangan character varying(255)
);
    DROP TABLE public.tbl_bobot;
       public         heap    postgres    false            �            1259    24971 
   tbl_gejala    TABLE     x   CREATE TABLE public.tbl_gejala (
    id_gejala character varying(4) NOT NULL,
    nama_gejala character varying(255)
);
    DROP TABLE public.tbl_gejala;
       public         heap    postgres    false            �            1259    24974    tbl_kerusakan    TABLE     �   CREATE TABLE public.tbl_kerusakan (
    id_kerusakan character varying(3) NOT NULL,
    nama_kerusakan character varying(255) NOT NULL,
    img character varying(255)
);
 !   DROP TABLE public.tbl_kerusakan;
       public         heap    postgres    false            �            1259    24979    tbl_pengguna    TABLE     �   CREATE TABLE public.tbl_pengguna (
    id_pengguna integer NOT NULL,
    nama character varying(255),
    email character varying(255),
    password character varying(255)
);
     DROP TABLE public.tbl_pengguna;
       public         heap    postgres    false            �            1259    24984    tbl_pengguna_id_pengguna_seq    SEQUENCE     �   CREATE SEQUENCE public.tbl_pengguna_id_pengguna_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.tbl_pengguna_id_pengguna_seq;
       public          postgres    false    218                       0    0    tbl_pengguna_id_pengguna_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.tbl_pengguna_id_pengguna_seq OWNED BY public.tbl_pengguna.id_pengguna;
          public          postgres    false    219            u           2604    24985    tbl_pengguna id_pengguna    DEFAULT     �   ALTER TABLE ONLY public.tbl_pengguna ALTER COLUMN id_pengguna SET DEFAULT nextval('public.tbl_pengguna_id_pengguna_seq'::regclass);
 G   ALTER TABLE public.tbl_pengguna ALTER COLUMN id_pengguna DROP DEFAULT;
       public          postgres    false    219    218                      0    24965 
   tbl_aturan 
   TABLE DATA           R   COPY public.tbl_aturan (id_aturan, id_kerusakan, id_gejala, id_bobot) FROM stdin;
    public          postgres    false    214   �                 0    24968 	   tbl_bobot 
   TABLE DATA           F   COPY public.tbl_bobot (id_bobot, nilai_bobot, keterangan) FROM stdin;
    public          postgres    false    215   �                 0    24971 
   tbl_gejala 
   TABLE DATA           <   COPY public.tbl_gejala (id_gejala, nama_gejala) FROM stdin;
    public          postgres    false    216   ;                 0    24974    tbl_kerusakan 
   TABLE DATA           J   COPY public.tbl_kerusakan (id_kerusakan, nama_kerusakan, img) FROM stdin;
    public          postgres    false    217   �!                 0    24979    tbl_pengguna 
   TABLE DATA           J   COPY public.tbl_pengguna (id_pengguna, nama, email, password) FROM stdin;
    public          postgres    false    218   �"                  0    0    tbl_pengguna_id_pengguna_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.tbl_pengguna_id_pengguna_seq', 1, true);
          public          postgres    false    219            w           2606    24987    tbl_aturan tbl_aturan_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY public.tbl_aturan
    ADD CONSTRAINT tbl_aturan_pkey PRIMARY KEY (id_aturan);
 D   ALTER TABLE ONLY public.tbl_aturan DROP CONSTRAINT tbl_aturan_pkey;
       public            postgres    false    214            y           2606    24989    tbl_bobot tbl_bobot_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.tbl_bobot
    ADD CONSTRAINT tbl_bobot_pkey PRIMARY KEY (id_bobot);
 B   ALTER TABLE ONLY public.tbl_bobot DROP CONSTRAINT tbl_bobot_pkey;
       public            postgres    false    215            {           2606    24991    tbl_gejala tbl_gejala_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY public.tbl_gejala
    ADD CONSTRAINT tbl_gejala_pkey PRIMARY KEY (id_gejala);
 D   ALTER TABLE ONLY public.tbl_gejala DROP CONSTRAINT tbl_gejala_pkey;
       public            postgres    false    216            }           2606    24993     tbl_kerusakan tbl_kerusakan_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.tbl_kerusakan
    ADD CONSTRAINT tbl_kerusakan_pkey PRIMARY KEY (id_kerusakan);
 J   ALTER TABLE ONLY public.tbl_kerusakan DROP CONSTRAINT tbl_kerusakan_pkey;
       public            postgres    false    217                       2606    24995    tbl_pengguna tbl_pengguna_pkey 
   CONSTRAINT     e   ALTER TABLE ONLY public.tbl_pengguna
    ADD CONSTRAINT tbl_pengguna_pkey PRIMARY KEY (id_pengguna);
 H   ALTER TABLE ONLY public.tbl_pengguna DROP CONSTRAINT tbl_pengguna_pkey;
       public            postgres    false    218            �           2606    24996 #   tbl_aturan tbl_aturan_id_bobot_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.tbl_aturan
    ADD CONSTRAINT tbl_aturan_id_bobot_fkey FOREIGN KEY (id_bobot) REFERENCES public.tbl_bobot(id_bobot);
 M   ALTER TABLE ONLY public.tbl_aturan DROP CONSTRAINT tbl_aturan_id_bobot_fkey;
       public          postgres    false    214    215    3193            �           2606    25001 $   tbl_aturan tbl_aturan_id_gejala_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.tbl_aturan
    ADD CONSTRAINT tbl_aturan_id_gejala_fkey FOREIGN KEY (id_gejala) REFERENCES public.tbl_gejala(id_gejala);
 N   ALTER TABLE ONLY public.tbl_aturan DROP CONSTRAINT tbl_aturan_id_gejala_fkey;
       public          postgres    false    3195    216    214            �           2606    25006 '   tbl_aturan tbl_aturan_id_kerusakan_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.tbl_aturan
    ADD CONSTRAINT tbl_aturan_id_kerusakan_fkey FOREIGN KEY (id_kerusakan) REFERENCES public.tbl_kerusakan(id_kerusakan);
 Q   ALTER TABLE ONLY public.tbl_aturan DROP CONSTRAINT tbl_aturan_id_kerusakan_fkey;
       public          postgres    false    3197    214    217               �   x�M�9��0D�X:̀�pK'Q0���9L�����Q]ҫ���y��-q�Jq��ߐ@.���4��@׌�&3�,��YO� 8��@HA�`'��������6|fx��0L�N����PO@�x�m�j�;P5c'��`��Q �z��P��
��4vҶ@�E�4:��b j,&�y%w�������iƁ,���d=���'���}�o�k�K         B   x�s20�4��LI�V�L����r20�4�3�NM���,��E�8�K�K�b&@1N/F��� Pa�         �  x�mT�n�0<�_�c{paI���pQ7��襗���R��GZ�}���H��$��3��Z�2��vҳS����d=��7;
�I�:�2�����P#uk+�,Ii��S�=wdG�<��7��v���Bl��%2���č��t����&���^9y�͛�j%M��۱�@��O߉˖\u��.�T-i��u6���m� ��3�DM��d�Z�I��"_PWy}�3+��&Vt8Y���Eh2j�ؕ���l!V&��;�Dky�}ͻd��9�o�t:��V��&#nʨIRg��gwb�|�&�����74l9�<�����:���`o�W+�6�4y�#��k� #�_+�J�.u8�s�A��9B��_5܁���3��C�/�j��\:����&��Z�^n�� �95�?�V�~Ҟ1�Vz�����E���9B�ogZ���l$#���#�x����b&���!���n���qa��?xy�T��i����2Kn�?�i��|t�C��x����N�$��*���\�حdׂ�im]'��%袧�k)����)���ޯR)���Xq#�Q�&�ٰ�_ hdoO��rur��*��}��ܰ1����\����*�������ش�u�F��/�6i<家�5n.��G#Y@�2��3�ۺE���ɯO���{;Ō         �   x����
�0���S�Z��ݓ�B��emCM[�e7=���b��ev�vvu����ЃS�P�U(mX�Q�0x�;l�.�)� �X���5��c�V���'���Ht�\��#<��u޵I��`�ꄆ���?�mSܹ�������v�J�ZS��N2�$ߛ.���	&��         ^   x�3�tL����L����9z����*F�*�*Q���N%�eƹ��!�f�~I^��N���a�������N�^&Ae��I���\1z\\\ ̠1     