-- membuat tabel gejala
CREATE TABLE
  IF NOT EXISTS tbl_gejala (id_gejala varchar(4) NOT NULL PRIMARY KEY, nama_gejala varchar(255));

-- membuat tabel bobot
CREATE TABLE
  IF NOT EXISTS tbl_bobot (
    id_bobot varchar(3) NOT NULL PRIMARY KEY,
    nilai_bobot float (11),
    keterangan varchar(255)
  );

--  membuat tabel kerusakan
CREATE TABLE
  IF NOT EXISTS tbl_kerusakan (
    id_kerusakan varchar(3) Not NULL PRIMARY KEY,
    nama_kerusakan varchar(255)
  );

--  membuat tabel rules
CREATE TABLE
  IF NOT EXISTS tbl_aturan (
    id_aturan varchar(4) PRIMARY KEY,
    id_kerusakan varchar(3),
    FOREIGN KEY (id_kerusakan) REFERENCES tbl_kerusakan (id_kerusakan),
    id_gejala varchar(4),
    FOREIGN KEY (id_gejala) REFERENCES tbl_gejala (id_gejala),
    id_bobot varchar(3),
    FOREIGN KEY (id_bobot) REFERENCES tbl_bobot (id_bobot)
  );

--  membuat tabel admin
CREATE TABLE
  tbl_pengguna (
    id_pengguna serial PRIMARY KEY,
    nama varchar(255),
    email varchar(255),
    password varchar(255)
  );

--  mengisi nilai tabel bobot
INSERT INTO
  tbl_bobot (id_bobot, nilai_bobot, keterangan)
VALUES
  ('B01', 0, 'Tidak Yakin'),
  ('B02', 0.4, 'Sedikit Yakin'),
  ('B03', 0.6, 'Cukup Yakin'),
  ('B04', 0.8, 'Yakin');

--  mengisi nilai tabel kerusakan
INSERT INTO
  tbl_kerusakan (id_kerusakan, nama_kerusakan)
VALUES
  ('K01', 'Kerusakan terjadi pada Kipas'),
  ('K02', 'Kerusakan terjadi pada Hardisk'),
  ('K03', 'Kerusakan terjadi pada Processor'),
  ('K04', 'Kerusakan terjadi pada Layar'),
  ('K05', 'Kerusakan terjadi pada Sistem Operasi'),
  ('K06', 'Kerusakan terjadi pada USB'),
  ('K07', 'Kerusakan terjadi pada Sistem Pengecasan');

--  mengisi nilai tabel gejala (tanpa nilai bobot pakar)
INSERT INTO
  tbl_gejala (id_gejala, nama_gejala)
VALUES
  ('G01', 'Laptop sering hang'),
  (
    'G02',
    'Temperatur kipas tidak dalam kondisi baik'
  ),
  ('G03', 'Laptop cepat panas'),
  ('G04', 'Laptop sering overheat'),
  ('G05', 'Kipas berisik'),
  ('G06', 'Terdapat angin di lubang sirkulasi'),
  ('G07', 'Laptop sering mati tiba-tiba'),
  ('G08', 'Kipas tidak bersirkulasi'),
  ('G09', 'Keyboard tidak dapat digunakan'),
  ('G10', 'Terdapat kode aneh'),
  ('G11', 'Tampil kode error pada laptop'),
  ('G12', 'Hardisk overheat'),
  ('G13', 'Hardisk mudah panas'),
  ('G14', 'Data seringkali tidak terbaca'),
  ('G15', 'Bluescreen'),
  ('G16', 'Laptop tidak dapat bekerja'),
  ('G17', '---'),
  ('G18', 'Gagal membuka aplikasi'),
  ('G19', 'Sistem hanya aktif sementara (sebentar)'),
  ('G20', 'Sering gagal booting'),
  ('G21', 'Layar bergetar'),
  ('G22', 'Terdapat bintik putih pada layar'),
  ('G23', 'Layar bergaris'),
  ('G24', 'Terdapat artifact pada layar'),
  ('G25', 'Layar blank'),
  ('G26', 'Booting terhenti setelah proses POST'),
  (
    'G27',
    'Booting ke windows berjalan sangat lambat'
  ),
  ('G28', 'Windows explorer tidak dapat dibuka'),
  ('G29', 'Start Menu tidak bekerja'),
  (
    'G30',
    'Proses Shutdown tidak berjalan dengan baik'
  ),
  (
    'G31',
    'Proses Shutdown berhenti sebelum komputer benar-benar mati'
  ),
  ('G32', 'Layar selalu senyap'),
  ('G33', 'Driver ter-uninstall'),
  ('G34', 'USB port longgar'),
  ('G35', 'USB port rusak'),
  ('G36', 'Jalur USB di PCB terputus'),
  ('G37', 'Tidak bisa menyeleksi USB'),
  ('G38', 'Proses charging bermasalah'),
  ('G39', 'Charger laptop bermasalah'),
  ('G40', 'Adaptop charger bermasalah'),
  ('G41', 'Kabel charger bermasalah'),
  ('G42', 'Konektor charger bermasalah'),
  ('G43', 'Port charger pada laptop bermasalah'),
  ('G44', 'Muncul tanda silang pada logo baterai');


-- mengisi nilai tabel aturan
INSERT INTO
  tbl_aturan (id_aturan, id_kerusakan, id_gejala, id_bobot)
VALUES
  ('R001', 'K01', 'G01', 'B03'),
  ('R002', 'K01', 'G02', 'B03'),
  ('R003', 'K01', 'G03', 'B02'),
  ('R004', 'K01', 'G04', 'B04'),
  ('R005', 'K01', 'G05', 'B04'),
  ('R006', 'K01', 'G06', 'B03'),
  ('R007', 'K01', 'G07', 'B02'),
  ('R008', 'K01', 'G08', 'B02'),
  ('R009', 'K01', 'G09', 'B04'),
  ('R010', 'K02', 'G10', 'B04'),
  ('R011', 'K02', 'G11', 'B04'),
  ('R012', 'K02', 'G12', 'B03'),
  ('R013', 'K02', 'G13', 'B03'),
  ('R014', 'K02', 'G14', 'B03'),
  ('R015', 'K02', 'G15', 'B03'),
  ('R016', 'K03', 'G01', 'B03'),
  ('R017', 'K03', 'G03', 'B02'),
  ('R018', 'K03', 'G07', 'B02'),
  ('R019', 'K03', 'G15', 'B03'),
  ('R020', 'K03', 'G16', 'B03'),
  ('R021', 'K03', 'G18', 'B03'),
  ('R022', 'K03', 'G19', 'B03'),
  ('R023', 'K04', 'G21', 'B02'),
  ('R024', 'K04', 'G22', 'B03'),
  ('R025', 'K04', 'G23', 'B04'),
  ('R026', 'K04', 'G24', 'B04'),
  ('R027', 'K04', 'G25', 'B02'),
  ('R028', 'K05', 'G26', 'B02'),
  ('R029', 'K05', 'G27', 'B03'),
  ('R030', 'K05', 'G28', 'B03'),
  ('R031', 'K05', 'G29', 'B04'),
  ('R032', 'K05', 'G30', 'B04'),
  ('R033', 'K05', 'G31', 'B03'),
  ('R034', 'K05', 'G32', 'B03'),
  ('R035', 'K06', 'G33', 'B02'),
  ('R036', 'K06', 'G34', 'B04'),
  ('R037', 'K06', 'G35', 'B03'),
  ('R038', 'K06', 'G36', 'B03'),
  ('R039', 'K06', 'G37', 'B04'),
  ('R040', 'K07', 'G03', 'B02'),
  ('R041', 'K07', 'G38', 'B04'),
  ('R042', 'K07', 'G39', 'B03'),
  ('R043', 'K07', 'G40', 'B03'),
  ('R044', 'K07', 'G41', 'B04'),
  ('R045', 'K07', 'G42', 'B03'),
  ('R046', 'K07', 'G43', 'B04'),
  ('R047', 'K07', 'G44', 'B04');

--  mencoba ambil data multi tabel
-- SELECT
--   tbl_aturan.id_aturan,
--   tbl_kerusakan.nama_kerusakan,
--   tbl_bobot.nilai_bobot
-- FROM
--   tbl_aturan,
--   tbl_kerusakan,
--   tbl_bobot
-- WHERE
--   tbl_kerusakan.id_kerusakan = tbl_aturan.id_kerusakan
--   AND tbl_bobot.id_bobot = tbl_aturan.id_bobot