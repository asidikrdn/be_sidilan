# Back End Aplikasi Sistem Diagnosa Laptop dan Notebook

  Aplikasi Sistem Diagnosa Laptop dan Notebook ini dibangun menggunakan bahasa pemrograman `Go` sebagai stack utama, dan relational database `PostgreeSQK` untuk menyimpan data.

## Cara Menggunakan Aplikasi

Berikut cara menjalankan aplikasi ini di komputer lokal

- Pastikan sudah menginstall `Go Compiler`, `PostgreeSQL` dan `Git SCM` di komputer anda
- Clone repository ini dengan menjalankan `git clone https://gitlab.com/asidikrdn/be_sidilan` di terminal/cmd
- Buka aplikasi pgAdmin 4, lalu buat database baru di PostgreeSQL dengan nama `db_sidilan`
- Klik kanan pada database `db_sidilan`, pilih `Restore`, pada bagian `Filename` silahkan klik dan pilih file `be_sidilan/DATABASE/backup-db_sidilan.sql` untuk me-restore database
- Cek database dan isinya dengan menjalankan perintah `SELECT * FROM tbl_kerusakan` dan `SELECT * FROM tb_pengguna` pada query tool di pgAdmin 4, jika muncul dan tidak error, bisa lanjut ke tahap berikutnya
- Masuk ke folder `be_sidilan`
- Buat file `.env` lalu isikan :
  - `DATABASE_URL="postgres://user:password@localhost/db_sidilan"`. Pada bagian `user` dan `password` silahkan disesuaikan dengan user dan password pada PostgreeSQL masing-masing
  - `APPLICATION_NAME="My Simple JWT App"`
  - `JWT_SIGNATURE_KEY="MySecretKey"`
- Selanjutnya buka terminal pada folder `be_sidilan` dan jalankan perintah `go run main.go`

## User Account

Nama      : `Admin`\
Email     : `admin@mail.com`\
Password  : `admin123`
