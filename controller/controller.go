package controller

import (
	"be_sidilan/models"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	jwt "github.com/golang-jwt/jwt/v4"
	"golang.org/x/crypto/bcrypt"
)

// mengambil nilai APPLICATION_NAME dari .env
var APPLICATION_NAME = os.Getenv("APPLICATION_NAME")

// mengambil nilai JWT_SIGNATURE_KEY dari .env lalu di parsing ke dalam bentuk byte
// var JWT_SIGNATURE_KEY = []byte(os.Getenv("JWT_SIGNATURE_KEY"))

// men-set durasi expire dari token jwt yang akan digenerate (1 jam)
var LOGIN_EXPIRATION_DURATION = time.Duration(1) * time.Hour

// men-set SigningMethodHS256 sebagai method yang digunakan dalam proses signing token jwt
var JWT_SIGNING_METHOD = jwt.SigningMethodHS256

// fungsi untuk menampilkan list kerusakan
func HandleDetailKerusakan(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	if r.Method == "GET" {

		listKerusakan := models.GetKerusakan()

		type detail_kerusakan struct {
			Id_kerusakan      string                    `json:"id_kerusakan"`
			Nama_kerusakan    string                    `json:"nama_kerusakan"`
			Img               string                    `json:"img"`
			Atribut_kerusakan []models.AtributKerusakan `json:"atribut_kerusakan"`
		}

		var DetailKerusakan []detail_kerusakan

		for _, data := range listKerusakan {
			var kerusakan detail_kerusakan
			kerusakan.Id_kerusakan = data.Id_kerusakan
			kerusakan.Nama_kerusakan = data.Nama_kerusakan
			if r.TLS == nil {
				// the scheme was HTTP
				kerusakan.Img = fmt.Sprintf("http://%s/static/img/%s", r.Host, data.Img)
			} else {
				// the scheme was HTTPS
				kerusakan.Img = fmt.Sprintf("https://%s/static/img/%s", r.Host, data.Img)
			}
			kerusakan.Atribut_kerusakan = models.GetDetailKerusakan(data.Id_kerusakan)

			DetailKerusakan = append(DetailKerusakan, kerusakan)
		}

		json.NewEncoder(w).Encode(DetailKerusakan)
		return
	}
}

// fungsi untuk menampilkan list gejala
func HandleGejala(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	// w.Header().Set("Access-Control-Allow-Origin", "*")
	// w.Header().Set("Access-Control-Allow-Origin", "http://localhost:3000")
	// w.Header().Set("Access-Control-Allow-Credentials", "true")

	if r.Method == "GET" {

		json.NewEncoder(w).Encode(models.GetGejala())
		return
	}
}

// fungsi untuk menampilkan list gejala
func HandleBobot(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	// w.Header().Set("Access-Control-Allow-Origin", "*")
	// w.Header().Set("Access-Control-Allow-Origin", "http://localhost:3000")
	// w.Header().Set("Access-Control-Allow-Credentials", "true")

	if r.Method == "GET" {

		json.NewEncoder(w).Encode(models.GetBobot())
		return
	}
}

// fungsi untuk menampilkan hasil score diagnosa berdasarkan inputan yang diterima pada parameter body
func HandleDiagnosa(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {

		var Data_Input_Diagnosa []models.Input_diagnosa

		// fmt.Println(r.Body)

		err := json.NewDecoder(r.Body).Decode(&Data_Input_Diagnosa)
		if err != nil {
			panic(err.Error())
		}

		// fmt.Println(Data_Input_User)
		// fmt.Println(models.Gejala())

		// fungsi scoring kerusakan dengan parameternya berupa input yang didapat dari parameter body
		scoringKerusakan := func(inputUser []models.Input_diagnosa) []models.Score_kerusakan {
			listKerusakan := models.GetKerusakan()

			// array penampung data scoring kerusakan, berisi kumpulan element nama_kerusakan dan score_kerusakan (sesuai pada struct score_kerusakan)
			var ArrDataScoreKerusakan []models.Score_kerusakan

			// pada setiap kerusakan
			for _, k := range listKerusakan {
				// var untuk menampung satu nama kerusakan dan satu score kerusakan
				var data_score_kerusakan models.Score_kerusakan

				// set nama_kerusakan dengan nama kerusakan yang di dapat dari list kerusakan saat ini
				data_score_kerusakan.Nama_kerusakan = k.Nama_kerusakan

				// ambil rules sesuai dengan kerusakan saat ini
				data_rules := models.GetRules(k.Id_kerusakan)

				// variabel score sementara
				var temp_score float64 = 0.0

				// pada tiap baris rules untuk kerusakan saat ini
				for _, rules := range data_rules {
					// set bobot pakar sesuai dengan nilai bobot yang didapat dari rules
					bobotPakar := rules.Nilai_bobot

					// set bobot user (nilai default 0)
					var bobotUser float64 = 0.0

					// pada setiap data input (gejala dan bobot)
					for _, input := range inputUser {
						// apabila terdapat gejala yang sesuai dengan gejala dari rules saat ini
						if input.Id_gejala == rules.Id_gejala {
							// set bobot user sesuai dengan data yang diinput
							bobotUser = models.GetNilaiBobot(input.Id_bobot)
						}
						// apabila tidak ada, maka bobotUser akan menggunakan nilai defaultnya (0)
					}

					// menghitung bobot kombinasi dan menyimpannya dalam variabel
					var bobotKombinasi float64 = bobotPakar * bobotUser

					// menghitung score dari tiap bobot kombinasi
					temp_score = temp_score + bobotKombinasi*(1-temp_score)
				}

				// setelah perulangan selesai, maka akan didapatkan score akhir untuk kerusakan saat ini, simpan nilai tersebut ke property Score_kerusakan milik object data_score_kerusakan dalam bentuk persen
				data_score_kerusakan.Score_kerusakan = temp_score / 1 * 100

				// push element data_score_kerusakan saat ini ke ArrDataScoreKerusakan
				ArrDataScoreKerusakan = append(ArrDataScoreKerusakan, data_score_kerusakan)
			}

			return ArrDataScoreKerusakan
		}

		json.NewEncoder(w).Encode(scoringKerusakan(Data_Input_Diagnosa))
		return
	}
}

func HandleRegister(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {

		r.ParseForm()

		var Register_User models.User
		Register_User.Nama = r.FormValue("nama")
		Register_User.Email = r.FormValue("email")

		// enkripsi password dengan bcrypt
		Register_User.Password, _ = bcrypt.GenerateFromPassword([]byte(r.FormValue("password")), 10)

		models.AddUser(Register_User)

		json.NewEncoder(w).Encode("New User has been added")
		return
	}
}

func HandleLogin(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		// parsing data dari form post
		// r.ParseForm()
		r.ParseMultipartForm(1024)

		// mengambil data dari form
		email := r.FormValue("email")
		password := r.FormValue("password")

		// fmt.Println(email)
		// fmt.Println(password)

		// membuat fungsi untuk validasi email dan password
		var authenticateUser = func(email string, password string) (bool, models.M) {
			UserData, err := models.GetUser(email)
			if err != nil {
				return false, nil
			}
			// compare password yang terdaftar dengan password yang diinputkan saat login
			errComparePassword := bcrypt.CompareHashAndPassword([]byte(UserData.Password), []byte(password))
			if errComparePassword != nil {
				return false, nil
			}

			var User = models.M{
				"id_pengguna": UserData.Id_pengguna,
				"nama":        UserData.Nama,
				"email":       UserData.Email,
			}
			return true, User
		}

		// menjalankan fungsi validasi
		ok, userInfo := authenticateUser(email, password)
		if !ok {
			// http.Error(w, "Invalid email or password", http.StatusBadRequest)
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode("Invalid email or password")
			return
		}

		// membuat claims untuk keperluan otentikasi
		claims := models.MyClaims{
			StandardClaims: jwt.StandardClaims{
				Issuer:    APPLICATION_NAME,
				ExpiresAt: time.Now().Add(LOGIN_EXPIRATION_DURATION).Unix(),
			},
			Name:  userInfo["nama"].(string),
			Email: userInfo["email"].(string),
		}

		// membuat token baru
		token := jwt.NewWithClaims(
			JWT_SIGNING_METHOD,
			claims,
		)

		// men-signed token yang sudah dibuat
		// signedToken, err := token.SignedString(JWT_SIGNATURE_KEY)
		signedToken, err := token.SignedString([]byte(os.Getenv("JWT_SIGNATURE_KEY")))
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		// mengirim token sebagai response v1
		json.NewEncoder(w).Encode(models.M{"token": signedToken})

		// mengirim token sebagai response v2
		// tokenString, _ := json.Marshal(models.M{"token": signedToken})
		// w.Write([]byte(tokenString))

		// mengirim token sebagai response v3
		// tokenString, _ := json.Marshal(models.M{"token": signedToken})
		// w.Write(tokenString)

		return
	}
}

func HandleAuth(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	// mengambil niali authorization dari request header
	authorizationHeader := r.Header.Get("Authorization")
	// fmt.Println(authorizationHeader)

	// cek apakah dalam authorization terdapat string Bearer
	if !strings.Contains(authorizationHeader, "Bearer") {
		http.Error(w, "Invalid token", http.StatusBadRequest)
		return
	}

	// menghilangkan string `Bearer ` pada authorization
	tokenString := strings.Replace(authorizationHeader, "Bearer ", "", -1)

	// meng-compare token jwt
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if method, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("signing method invalid")
		} else if method != JWT_SIGNING_METHOD {
			return nil, fmt.Errorf("signing method invalid")
		}

		return []byte(os.Getenv("JWT_SIGNATURE_KEY")), nil
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// mengambil informasi dari bagian claims pada jwt
	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok || !token.Valid {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// mengirim informasi claims sebagai response
	json.NewEncoder(w).Encode(claims)
}

func HandleDeleteRules(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		w.Header().Set("Content-Type", "application/json")

		// mengambil niali authorization dari request header
		authorizationHeader := r.Header.Get("Authorization")
		// fmt.Println(authorizationHeader)

		// cek apakah dalam authorization terdapat string Bearer
		if !strings.Contains(authorizationHeader, "Bearer") {
			http.Error(w, "Invalid token", http.StatusBadRequest)
			return
		}

		// menghilangkan string `Bearer ` pada authorization
		tokenString := strings.Replace(authorizationHeader, "Bearer ", "", -1)

		// meng-compare token jwt
		_, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			if method, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("signing method invalid")
			} else if method != JWT_SIGNING_METHOD {
				return nil, fmt.Errorf("signing method invalid")
			}

			return []byte(os.Getenv("JWT_SIGNATURE_KEY")), nil
		})
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		var id_aturan string

		err = json.NewDecoder(r.Body).Decode(&id_aturan)
		if err != nil {
			panic(err.Error())
		}

		models.DeleteRules(id_aturan)
		return
	}
}

func HandleEditRules(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		w.Header().Set("Content-Type", "application/json")

		// mengambil niali authorization dari request header
		authorizationHeader := r.Header.Get("Authorization")
		// fmt.Println(authorizationHeader)

		// cek apakah dalam authorization terdapat string Bearer
		if !strings.Contains(authorizationHeader, "Bearer") {
			http.Error(w, "Invalid token", http.StatusBadRequest)
			return
		}

		// menghilangkan string `Bearer ` pada authorization
		tokenString := strings.Replace(authorizationHeader, "Bearer ", "", -1)

		// meng-compare token jwt
		_, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			if method, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("signing method invalid")
			} else if method != JWT_SIGNING_METHOD {
				return nil, fmt.Errorf("signing method invalid")
			}

			return []byte(os.Getenv("JWT_SIGNATURE_KEY")), nil
		})
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		var aturan models.Rules

		err = json.NewDecoder(r.Body).Decode(&aturan)
		if err != nil {
			panic(err.Error())
		}

		models.EditRules(aturan)
		return
	}
}

func HandleAddRules(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		w.Header().Set("Content-Type", "application/json")

		// mengambil niali authorization dari request header
		authorizationHeader := r.Header.Get("Authorization")
		// fmt.Println(authorizationHeader)

		// cek apakah dalam authorization terdapat string Bearer
		if !strings.Contains(authorizationHeader, "Bearer") {
			http.Error(w, "Invalid token", http.StatusBadRequest)
			return
		}

		// menghilangkan string `Bearer ` pada authorization
		tokenString := strings.Replace(authorizationHeader, "Bearer ", "", -1)

		// meng-compare token jwt
		_, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			if method, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("signing method invalid")
			} else if method != JWT_SIGNING_METHOD {
				return nil, fmt.Errorf("signing method invalid")
			}

			return []byte(os.Getenv("JWT_SIGNATURE_KEY")), nil
		})
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		var aturan models.Rules

		err = json.NewDecoder(r.Body).Decode(&aturan)
		if err != nil {
			panic(err.Error())
		}

		models.AddRules(aturan)
		return
	}
}

func HandleAddGejala(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		w.Header().Set("Content-Type", "application/json")

		// mengambil niali authorization dari request header
		authorizationHeader := r.Header.Get("Authorization")
		// fmt.Println(authorizationHeader)

		// cek apakah dalam authorization terdapat string Bearer
		if !strings.Contains(authorizationHeader, "Bearer") {
			http.Error(w, "Invalid token", http.StatusBadRequest)
			return
		}

		// menghilangkan string `Bearer ` pada authorization
		tokenString := strings.Replace(authorizationHeader, "Bearer ", "", -1)

		// meng-compare token jwt
		_, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			if method, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("signing method invalid")
			} else if method != JWT_SIGNING_METHOD {
				return nil, fmt.Errorf("signing method invalid")
			}

			return []byte(os.Getenv("JWT_SIGNATURE_KEY")), nil
		})
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		var nama_gejala string

		err = json.NewDecoder(r.Body).Decode(&nama_gejala)
		if err != nil {
			panic(err.Error())
		}

		models.AddGejala(nama_gejala)
		return
	}
}

func HandleAddKerusakan(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		w.Header().Set("Content-Type", "application/json")

		// mengambil niali authorization dari request header
		authorizationHeader := r.Header.Get("Authorization")
		// fmt.Println(authorizationHeader)

		// cek apakah dalam authorization terdapat string Bearer
		if !strings.Contains(authorizationHeader, "Bearer") {
			http.Error(w, "Invalid token", http.StatusBadRequest)
			return
		}

		// menghilangkan string `Bearer ` pada authorization
		tokenString := strings.Replace(authorizationHeader, "Bearer ", "", -1)

		// meng-compare token jwt
		_, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			if method, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("signing method invalid")
			} else if method != JWT_SIGNING_METHOD {
				return nil, fmt.Errorf("signing method invalid")
			}

			return []byte(os.Getenv("JWT_SIGNATURE_KEY")), nil
		})
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		r.ParseMultipartForm(1024)

		nama_kerusakan := r.FormValue("nama-kerusakan")

		// membuat sebuah var untuk digunakan sebagai key pada context (untuk mengatasi warning should not use built-in type string as key)
		var UploadFileID models.ContextKey = "FileName"

		// mengambil data dari context yang dikirim oleh middleware.UploadFile
		dataContex := r.Context().Value(UploadFileID)

		// menggunakan filename yang didapat untuk mengisi property Img milik object project
		img_kerusakan := dataContex.(string)

		models.AddKerusakan(nama_kerusakan, img_kerusakan)
		return
	}
}
