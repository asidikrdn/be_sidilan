package main

import (
	"be_sidilan/router"
	"fmt"
	"log"
	"net/http"

	"github.com/joho/godotenv"
	"github.com/rs/cors"
)

func main() {
	router := router.Router()

	// import .env file
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	//	set up CORS middleware
	c := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		// AllowedHeaders:   []string{"Content-Type, Access-Control-Allow-Headers, Access-Control-Allow-Origin, Authorization, Origin, Accept, X-Requested-With"}, <-- ini error, liat kutipnya, kocak memang si aku ini
		AllowedHeaders:   []string{"Content-Type", "Authorization"},
		AllowedMethods:   []string{"OPTIONS", "GET", "POST"},
		AllowCredentials: true,
		// OptionsPassthrough: true,
		// Enable Debugging for testing, consider disabling in production
		// Debug: true,
	})

	// Add cors middleware on all route
	handler := c.Handler(router)

	fmt.Println("Server dijalankan pada port 4135...")
	http.ListenAndServe("localhost:4135", handler)
	// http.ListenAndServe("localhost:4135", nil)
}
