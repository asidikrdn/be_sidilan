package models

import (
	"be_sidilan/config"
	"context"
	"fmt"
	"strconv"
	"strings"

	jwt "github.com/golang-jwt/jwt/v4"
)

type ContextKey string

// struct untuk data kerusakan
type kerusakan struct {
	Id_kerusakan   string `json:"id_kerusakan"`
	Nama_kerusakan string `json:"nama_kerusakan"`
	Img            string `json:"img"`
}

// struct untuk data gejala
type gejala struct {
	Id_gejala   string `json:"id_gejala"`
	Nama_gejala string `json:"nama_gejala"`
}

// struct untuk data bobot
type bobot struct {
	Id_bobot    string  `json:"id_bobot"`
	Nilai_bobot float64 `json:"nilai_bobot"`
	Keterangan  string  `json:"keterangan"`
}

// struct untuk data rules
type Rules struct {
	Id_aturan    string  `json:"id_aturan"`
	Id_kerusakan string  `json:"id_kerusakan"`
	Id_gejala    string  `json:"id_gejala"`
	Id_bobot     string  `json:"id_bobot"`
	Nilai_bobot  float64 `json:"nilai_bobot"`
}

type AtributKerusakan struct {
	Id_aturan   string  `json:"id_aturan"`
	Nama_gejala string  `json:"nama_gejala"`
	Nilai_bobot float64 `json:"nilai_bobot"`
}

// struct untuk data yang diinputkan user
type Input_diagnosa struct {
	Id_gejala string `json:"id_gejala"`
	Id_bobot  string `json:"id_bobot"`
}

// struct untuk data score kerusakan
type Score_kerusakan struct {
	Nama_kerusakan  string  `json:"nama_kerusakan"`
	Score_kerusakan float64 `json:"score_kerusakan"`
}

type User struct {
	Id_pengguna int    `json:"id_pengguna"`
	Nama        string `json:"nama"`
	Email       string `json:"email"`
	Password    []byte `json:"password"`
}

// membuat tipe data M yang bentuknya adalah Object interface{} (digunakan untuk memudahkan agar tak perlu membuat map[string]interface{} berkali-kali)
type M map[string]interface{}

// membuat struct untuk tipe data dari claims
type MyClaims struct {
	jwt.StandardClaims
	Name  string `json:"name"`
	Email string `json:"Email"`
}

// var Data_Kerusakan = []kerusakan{
// 	{"k01", "Kerusakan CPU"},
// 	{"k02", "Kerusakan FAN"},
// 	{"k03", "Kerusakan Layar"},
// }

// var Data_Gejala = []gejala{
// 	{"g001", "Laptop mudah panas"},
// 	{"g002", "Laptop sering blank"},
// 	{"g003", "Laptop terasa lambat"},
// 	{"g004", "Kipas berisik"},
// }

func GetGejala() []gejala {
	// koneksi ke DB
	db, err := config.CreateConnection()
	if err != nil {
		panic(err.Error())
	}
	defer db.Close(context.Background())

	rows, err := db.Query(context.Background(), "SELECT * FROM tbl_gejala")
	if err != nil {
		panic(err.Error())

	}
	defer rows.Close()

	var DataGejala []gejala

	for rows.Next() {
		var data_gejala gejala

		err := rows.Scan(&data_gejala.Id_gejala, &data_gejala.Nama_gejala)
		if err != nil {
			panic(err.Error())
		}
		DataGejala = append(DataGejala, data_gejala)
	}

	if err = rows.Err(); err != nil {
		panic(err.Error())
	}

	return DataGejala
}

func GetBobot() []bobot {
	// koneksi ke DB
	db, err := config.CreateConnection()
	if err != nil {
		panic(err.Error())
	}
	defer db.Close(context.Background())

	rows, err := db.Query(context.Background(), "SELECT * FROM tbl_bobot")
	if err != nil {
		panic(err.Error())

	}
	defer rows.Close()

	var DataBobot []bobot

	for rows.Next() {
		var data_bobot bobot

		err := rows.Scan(&data_bobot.Id_bobot, &data_bobot.Nilai_bobot, &data_bobot.Keterangan)
		if err != nil {
			panic(err.Error())
		}
		DataBobot = append(DataBobot, data_bobot)
	}

	if err = rows.Err(); err != nil {
		panic(err.Error())
	}

	return DataBobot
}

func GetKerusakan() []kerusakan {
	// koneksi ke DB
	db, err := config.CreateConnection()
	if err != nil {
		panic(err.Error())
	}
	defer db.Close(context.Background())

	rows, err := db.Query(context.Background(), "SELECT * FROM tbl_kerusakan")
	if err != nil {
		panic(err.Error())

	}
	defer rows.Close()

	var DataKerusakan []kerusakan

	for rows.Next() {
		var data_kerusakan kerusakan

		err := rows.Scan(&data_kerusakan.Id_kerusakan, &data_kerusakan.Nama_kerusakan, &data_kerusakan.Img)
		if err != nil {
			panic(err.Error())
		}
		DataKerusakan = append(DataKerusakan, data_kerusakan)
	}

	if err = rows.Err(); err != nil {
		panic(err.Error())
	}

	return DataKerusakan
}

func GetRules(id_kerusakan string) []Rules {
	// koneksi ke DB
	db, err := config.CreateConnection()
	if err != nil {
		panic(err.Error())
	}
	defer db.Close(context.Background())

	rows, err := db.Query(context.Background(), `SELECT
  tbl_aturan.id_aturan,
	tbl_gejala.id_gejala,
  tbl_bobot.nilai_bobot
FROM
  tbl_aturan,
  tbl_gejala,
  tbl_bobot
WHERE
  tbl_aturan.id_kerusakan = $1
  AND tbl_gejala.id_gejala = tbl_aturan.id_gejala
  AND tbl_bobot.id_bobot = tbl_aturan.id_bobot
ORDER BY tbl_aturan.id_aturan ASC`, id_kerusakan)
	if err != nil {
		panic(err.Error())

	}
	defer rows.Close()

	var DataRules []Rules

	for rows.Next() {
		var data_rules Rules

		err := rows.Scan(&data_rules.Id_aturan, &data_rules.Id_gejala, &data_rules.Nilai_bobot)
		if err != nil {
			panic(err.Error())
		}
		DataRules = append(DataRules, data_rules)
	}

	if err = rows.Err(); err != nil {
		panic(err.Error())
	}

	return DataRules
}

func GetNilaiBobot(id_bobot string) float64 {
	// koneksi ke DB
	db, err := config.CreateConnection()
	if err != nil {
		panic(err.Error())
	}
	defer db.Close(context.Background())

	var nilai_bobot float64

	err = db.QueryRow(context.Background(), `SELECT
  tbl_bobot.nilai_bobot
FROM
  tbl_bobot
WHERE
  id_bobot = $1`, id_bobot).Scan(&nilai_bobot)
	if err != nil {
		panic(err.Error())

	}

	return nilai_bobot
}

func GetDetailKerusakan(id_kerusakan string) []AtributKerusakan {
	// koneksi ke DB
	db, err := config.CreateConnection()
	if err != nil {
		panic(err.Error())
	}
	defer db.Close(context.Background())

	rows, err := db.Query(context.Background(), `SELECT
	tbl_aturan.id_aturan,
	tbl_gejala.nama_gejala,
  tbl_bobot.nilai_bobot
FROM
	tbl_aturan,
	tbl_kerusakan,
	tbl_gejala,
	tbl_bobot
WHERE
  tbl_aturan.id_kerusakan = $1
  AND tbl_kerusakan.id_kerusakan = tbl_aturan.id_kerusakan
  AND tbl_gejala.id_gejala = tbl_aturan.id_gejala
  AND tbl_bobot.id_bobot = tbl_aturan.id_bobot
ORDER BY tbl_bobot.id_bobot ASC`, id_kerusakan)
	if err != nil {
		panic(err.Error())

	}
	defer rows.Close()

	var DetailKerusakan []AtributKerusakan

	for rows.Next() {
		var detail_kerusakan AtributKerusakan

		err := rows.Scan(&detail_kerusakan.Id_aturan, &detail_kerusakan.Nama_gejala, &detail_kerusakan.Nilai_bobot)
		if err != nil {
			panic(err.Error())
		}
		DetailKerusakan = append(DetailKerusakan, detail_kerusakan)
	}

	if err = rows.Err(); err != nil {
		panic(err.Error())
	}

	return DetailKerusakan
}

func AddUser(newUser User) {
	db, err := config.CreateConnection()
	if err != nil {
		panic(err.Error())
	}

	_, errQuery := db.Exec(context.Background(), "INSERT INTO tbl_pengguna (nama, email, password) VALUES ($1, $2, $3)", newUser.Nama, newUser.Email, newUser.Password)
	if errQuery != nil {
		panic(errQuery.Error())
	}
}

func GetUser(email string) (User, error) {
	db, err := config.CreateConnection()
	if err != nil {
		panic(err.Error())
	}

	var UserData User

	errScan := db.QueryRow(context.Background(), "SELECT * FROM tbl_pengguna WHERE email = $1", email).Scan(&UserData.Id_pengguna, &UserData.Nama, &UserData.Email, &UserData.Password)
	if errScan != nil {
		return UserData, errScan
	}

	return UserData, nil
}

func EditRules(new_rules Rules) {
	db, err := config.CreateConnection()
	if err != nil {
		panic(err.Error())
	}

	_, errQuery := db.Exec(context.Background(), "UPDATE tbl_aturan SET id_gejala=$1, id_bobot=$2 WHERE id_aturan=$3", new_rules.Id_gejala, new_rules.Id_bobot, new_rules.Id_aturan)
	if errQuery != nil {
		panic(errQuery.Error())
	}

	fmt.Println("Aturan dengan id " + new_rules.Id_aturan + " berhasil diupdate")
}

func DeleteRules(id_rules string) {
	db, err := config.CreateConnection()
	if err != nil {
		panic(err.Error())
	}

	_, errQuery := db.Exec(context.Background(), "DELETE FROM tbl_aturan WHERE id_aturan = $1", id_rules)
	if errQuery != nil {
		panic(errQuery.Error())
	}

	fmt.Println("Aturan dengan id " + id_rules + " berhasil dihapus")
}

func AddRules(new_rules Rules) {
	db, err := config.CreateConnection()
	if err != nil {
		panic(err.Error())
	}

	// mengambil id aturan terakhir
	rows, err := db.Query(context.Background(), `SELECT
		id_aturan
	FROM
		tbl_aturan
	ORDER BY
		id_aturan DESC`)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()
	var id_rules []string
	for rows.Next() {
		var id_rule string
		errScan := rows.Scan(&id_rule)
		if errScan != nil {
			panic(errScan.Error())
		}
		id_rules = append(id_rules, id_rule)
	}
	// fmt.Println(id_rules[0])

	// membuat id_aturan baru yang merupakan hasil id-aturan lama + 1
	arr_id_aturan := strings.Split(id_rules[0], "")
	text_id_aturan := arr_id_aturan[0]
	number_id_aturan := fmt.Sprintf("%s%s%s", arr_id_aturan[1], arr_id_aturan[2], arr_id_aturan[3])
	int_number_id_aturan, _ := strconv.Atoi(number_id_aturan)
	int_number_id_aturan += 1
	number_id_aturan = strconv.Itoa(int_number_id_aturan)
	if len(number_id_aturan) <= 2 {
		number_id_aturan = "0" + number_id_aturan
	}
	new_rules.Id_aturan = text_id_aturan + number_id_aturan
	fmt.Println(new_rules.Id_aturan)

	// menambahkan aturan baru
	_, errQuery := db.Exec(context.Background(), "INSERT INTO tbl_aturan (id_aturan, id_kerusakan, id_gejala, id_bobot) VALUES ($1, $2, $3, $4)", new_rules.Id_aturan, new_rules.Id_kerusakan, new_rules.Id_gejala, new_rules.Id_bobot)
	if errQuery != nil {
		panic(errQuery.Error())
	}

	fmt.Println("Berhasil menambahkan aturan baru")
}

func AddGejala(new_nama_gejala string) {
	db, err := config.CreateConnection()
	if err != nil {
		panic(err.Error())
	}

	// mengambil id aturan terakhir
	rows, err := db.Query(context.Background(), `SELECT
		id_gejala
	FROM
		tbl_gejala
	ORDER BY
		id_gejala DESC`)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()
	var gejala []string
	for rows.Next() {
		var id_gejala string
		errScan := rows.Scan(&id_gejala)
		if errScan != nil {
			panic(errScan.Error())
		}
		gejala = append(gejala, id_gejala)
	}
	// fmt.Println(gejala[0])

	// membuat id_gejala baru yang merupakan hasil id-gejala lama + 1
	arr_id_gejala := strings.Split(gejala[0], "")
	text_id_gejala := arr_id_gejala[0]
	number_id_gejala := fmt.Sprintf("%s%s", arr_id_gejala[1], arr_id_gejala[2])
	int_number_id_gejala, _ := strconv.Atoi(number_id_gejala)
	int_number_id_gejala += 1
	number_id_gejala = strconv.Itoa(int_number_id_gejala)
	// if len(number_id_gejala) <= 2 {
	// 	number_id_gejala = "0" + number_id_gejala
	// }
	new_id_gejala := text_id_gejala + number_id_gejala
	// fmt.Println(new_id_gejala)
	// fmt.Println(new_nama_gejala)

	// // menambahkan aturan baru
	_, errQuery := db.Exec(context.Background(), "INSERT INTO tbl_gejala (id_gejala, nama_gejala) VALUES ($1, $2)", new_id_gejala, new_nama_gejala)
	if errQuery != nil {
		panic(errQuery.Error())
	}

	fmt.Println("Berhasil menambahkan gejala baru")
}

func AddKerusakan(new_nama_kerusakan string, new_img_kerusakan string) {
	db, err := config.CreateConnection()
	if err != nil {
		panic(err.Error())
	}

	// mengambil id aturan terakhir
	rows, err := db.Query(context.Background(), `SELECT
		id_kerusakan
	FROM
		tbl_kerusakan
	ORDER BY
		id_kerusakan DESC`)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()
	var kerusakan []string
	for rows.Next() {
		var id_kerusakan string
		errScan := rows.Scan(&id_kerusakan)
		if errScan != nil {
			panic(errScan.Error())
		}
		kerusakan = append(kerusakan, id_kerusakan)
	}
	// fmt.Println(kerusakan[0])

	// membuat id_kerusakan baru yang merupakan hasil id-kerusakan lama + 1
	arr_id_kerusakan := strings.Split(kerusakan[0], "")
	text_id_kerusakan := arr_id_kerusakan[0]
	number_id_kerusakan := fmt.Sprintf("%s%s", arr_id_kerusakan[1], arr_id_kerusakan[2])
	int_number_id_kerusakan, _ := strconv.Atoi(number_id_kerusakan)
	int_number_id_kerusakan += 1
	number_id_kerusakan = strconv.Itoa(int_number_id_kerusakan)
	if len(number_id_kerusakan) <= 1 {
		number_id_kerusakan = "0" + number_id_kerusakan
	}
	new_id_kerusakan := text_id_kerusakan + number_id_kerusakan
	// fmt.Println(new_id_kerusakan)
	// fmt.Println(new_nama_kerusakan)
	// fmt.Println(new_img_kerusakan)

	// // menambahkan aturan baru
	_, errQuery := db.Exec(context.Background(), "INSERT INTO tbl_kerusakan (id_kerusakan, nama_kerusakan, img) VALUES ($1, $2, $3)", new_id_kerusakan, new_nama_kerusakan, new_img_kerusakan)
	if errQuery != nil {
		panic(errQuery.Error())
	}

	fmt.Println("Berhasil menambahkan kerusakan baru")
}
