package router

import (
	"be_sidilan/controller"
	"be_sidilan/middleware"
	"net/http"

	"github.com/gorilla/mux"
)

func Router() *mux.Router {
	router := mux.NewRouter()

	// membuat routing file server (berguna untuk mengakses file-file static/aset yang dibutuhkan, misalnya file css, gambar, js, dll)
	router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", // semua request yang masuk ke endpoint `/static` akan diarahkan ke actual handler
		http.FileServer(http.Dir("./assets")))) // actual handler

	router.HandleFunc("/api/kerusakan", controller.HandleDetailKerusakan).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/gejala", controller.HandleGejala).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/bobot", controller.HandleBobot).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/diagnosa", controller.HandleDiagnosa).Methods("POST", "OPTIONS")
	router.HandleFunc("/api/register", controller.HandleRegister).Methods("POST", "OPTIONS")
	router.HandleFunc("/api/login", controller.HandleLogin).Methods("POST", "OPTIONS")
	router.HandleFunc("/api/auth", controller.HandleAuth).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/editrules", controller.HandleEditRules).Methods("POST", "OPTIONS")
	router.HandleFunc("/api/deleterules", controller.HandleDeleteRules).Methods("POST", "OPTIONS")
	router.HandleFunc("/api/addrules", controller.HandleAddRules).Methods("POST", "OPTIONS")
	router.HandleFunc("/api/addgejala", controller.HandleAddGejala).Methods("POST", "OPTIONS")
	router.HandleFunc("/api/addkerusakan", middleware.UploadFile(controller.HandleAddKerusakan)).Methods("POST", "OPTIONS")

	return router
}
